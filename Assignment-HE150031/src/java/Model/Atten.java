/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.Date;

/**
 *
 * @author Admin
 */
public class Atten {
    private int attenID;
    private Schedule sche;
    private Account userid;
    private Date date;
    private Slot slot;
    private boolean attens;
    private String note;

    public Atten() {
    }

    public Atten(int attenID, Schedule sche, Account userid, Date date, Slot slot, boolean attens, String note) {
        this.attenID = attenID;
        this.sche = sche;
        this.userid = userid;
        this.date = date;
        this.slot = slot;
        this.attens = attens;
        this.note = note;
    }

    public int getAttenID() {
        return attenID;
    }

    public void setAttenID(int attenID) {
        this.attenID = attenID;
    }

    public Schedule getSche() {
        return sche;
    }

    public void setSche(Schedule sche) {
        this.sche = sche;
    }

    public Account getUserid() {
        return userid;
    }

    public void setUserid(Account userid) {
        this.userid = userid;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Slot getSlot() {
        return slot;
    }

    public void setSlot(Slot slot) {
        this.slot = slot;
    }

    public boolean isAttens() {
        return attens;
    }

    public void setAttens(boolean attens) {
        this.attens = attens;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

   
    
    }

    
